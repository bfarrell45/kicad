Teensy library for KiCAD
=======================================

This folder includes symbols and footprints for the following Teensy versions:

  - Teensy 1.0
  - Teensy++ 1.0
  - Teensy 2.0
  - Teensy++ 2.0
  - Teensy 3.0
  - Teensy 3.1
  - Teensy 3.2
  - Teensy 3.5
  - Teensy 3.6
  - Teensy 4.0
  - Teensy LC
