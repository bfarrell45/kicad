EESchema Schematic File Version 5
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Wire Bus Line
	4400 4000 4500 4000
Wire Bus Line
	4600 4250 4500 4250
Wire Wire Line
	5600 3700 5700 3700
Wire Wire Line
	5600 3800 5700 3800
Wire Bus Line
	5700 3900 5600 3900
Wire Bus Line
	7100 3550 7200 3550
Wire Bus Line
	4600 3900 4600 4050
Wire Bus Line
	5600 3900 5600 4050
Wire Wire Line
	4400 3700 4600 3700
Wire Bus Line
	4400 3800 4600 3800
Wire Bus Line
	4400 3900 4600 3900
Wire Bus Line
	4500 4250 4500 4000
Wire Wire Line
	4600 3700 4600 3450
Wire Wire Line
	5600 3450 5600 3700
Wire Wire Line
	7100 4150 7100 4400
Wire Wire Line
	6400 3850 6400 4150
Wire Wire Line
	5700 4000 5700 4400
Wire Wire Line
	4600 3450 5600 3450
Wire Bus Line
	5600 4050 4600 4050
Wire Wire Line
	7100 4400 5700 4400
Text HLabel 4400 3700 0    50   Input ~ 0
M1
Text HLabel 4400 3800 0    50   Input ~ 0
M2{SERVO}
Text HLabel 4400 3900 0    50   Input ~ 0
M3{SERVO}
Text HLabel 4400 4000 0    50   Input ~ 0
M4{SERVO}
Text HLabel 7200 3550 2    50   Input ~ 0
M5{SERVO}
$Comp
L Sprite_MotionPlatform:MS #U7
U 1 1 5DA7D079
P 4700 4150
F 0 "#U7" H 5128 4053 50  0000 L CNN
F 1 "MS" H 5128 3962 50  0000 L CNN
F 2 "" H 4750 4200 50  0001 C CNN
F 3 "" H 4750 4200 50  0001 C CNN
	1    4700 4150
	1    0    0    -1  
$EndComp
$Comp
L Sprite_MotionPlatform:MR #U9
U 1 1 5DA7D1CD
P 6500 3450
F 0 "#U9" H 6808 3615 50  0000 C CNN
F 1 "MR" H 6808 3524 50  0000 C CNN
F 2 "" H 6550 3500 50  0001 C CNN
F 3 "" H 6550 3500 50  0001 C CNN
	1    6500 3450
	1    0    0    -1  
$EndComp
$Comp
L Sprite_MotionPlatform:MM #U10
U 1 1 5DA7CF37
P 6500 4050
F 0 "#U10" H 6750 4215 50  0000 C CNN
F 1 "MM" H 6750 4124 50  0000 C CNN
F 2 "" H 6500 4050 50  0001 C CNN
F 3 "" H 6500 4050 50  0001 C CNN
	1    6500 4050
	1    0    0    -1  
$EndComp
$Comp
L Sprite_MotionPlatform:MB #U6
U 1 1 5DB14DDB
P 4700 3700
F 0 "#U6" H 5100 3865 50  0000 C CNN
F 1 "MB" H 5100 3774 50  0000 C CNN
F 2 "" H 4700 3700 50  0001 C CNN
F 3 "" H 4700 3700 50  0001 C CNN
	1    4700 3700
	1    0    0    -1  
$EndComp
$Comp
L Sprite_MotionPlatform:ME #U8
U 1 1 5DA7CDFF
P 5800 3550
F 0 "#U8" H 6050 3715 50  0000 C CNN
F 1 "ME" H 6050 3624 50  0000 C CNN
F 2 "" H 5850 3600 50  0001 C CNN
F 3 "" H 5850 3600 50  0001 C CNN
	1    5800 3550
	1    0    0    -1  
$EndComp
$EndSCHEMATC
