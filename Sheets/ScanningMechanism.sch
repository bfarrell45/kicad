EESchema Schematic File Version 5
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
NoConn ~ 5200 3650
NoConn ~ 5950 3300
NoConn ~ 5950 3750
Wire Bus Line
	6750 4050 6850 4050
Wire Bus Line
	4950 3450 5200 3450
Wire Bus Line
	4950 4500 5200 4500
Wire Bus Line
	4950 4650 5200 4650
Wire Wire Line
	5850 4500 6150 4500
Wire Bus Line
	5950 3600 6850 3600
Wire Wire Line
	6850 3450 5950 3450
Text HLabel 4950 3450 0    50   Input ~ 0
L1{PWR}
Text HLabel 4950 4500 0    50   Input ~ 0
L4{PWR}
Text HLabel 4950 4650 0    50   Input ~ 0
L5{SERVO}
Text HLabel 6850 3450 2    50   Input ~ 0
L3
Text HLabel 6850 3600 2    50   Input ~ 0
L2{UART}
Text HLabel 6850 4050 2    50   Input ~ 0
L6{QE}
$Comp
L Sprite_ScanningMechanism:LE #U13
U 1 1 5DA8FC7A
P 6250 3950
AR Path="/5DA8F5E4/5DA8FC7A" Ref="#U13"  Part="1" 
AR Path="/5DA7CA58/5DB30C0E/5DA8F5E4/5DA8FC7A" Ref="#U?"  Part="1" 
AR Path="/5DA7CA58/5DA8F5E4/5DA8FC7A" Ref="#U?"  Part="1" 
AR Path="/5DA7C9DA/5DAA0908/5DA8F5E4/5DA8FC7A" Ref="#U?"  Part="1" 
AR Path="/5DA7C9DA/5DA8F5E4/5DA8FC7A" Ref="#U?"  Part="1" 
AR Path="/5DA8B5E5/5DA8F5E4/5DA8FC7A" Ref="#U?"  Part="1" 
F 0 "#U13" H 6508 4115 50  0000 C CNN
F 1 "LE" H 6508 4024 50  0000 C CNN
F 2 "" H 6300 4000 50  0001 C CNN
F 3 "" H 6300 4000 50  0001 C CNN
	1    6250 3950
	1    0    0    -1  
$EndComp
$Comp
L Sprite_ScanningMechanism:LM #U14
U 1 1 5DA8F8BD
P 6250 4400
AR Path="/5DA8F5E4/5DA8F8BD" Ref="#U14"  Part="1" 
AR Path="/5DA7CA58/5DB30C0E/5DA8F5E4/5DA8F8BD" Ref="#U?"  Part="1" 
AR Path="/5DA7CA58/5DA8F5E4/5DA8F8BD" Ref="#U?"  Part="1" 
AR Path="/5DA7C9DA/5DAA0908/5DA8F5E4/5DA8F8BD" Ref="#U?"  Part="1" 
AR Path="/5DA7C9DA/5DA8F5E4/5DA8F8BD" Ref="#U?"  Part="1" 
AR Path="/5DA8B5E5/5DA8F5E4/5DA8F8BD" Ref="#U?"  Part="1" 
F 0 "#U14" H 6726 4303 50  0000 L CNN
F 1 "LM" H 6726 4212 50  0000 L CNN
F 2 "" H 6300 4450 50  0001 C CNN
F 3 "" H 6300 4450 50  0001 C CNN
	1    6250 4400
	1    0    0    -1  
$EndComp
$Comp
L Sprite_ScanningMechanism:LS #U12
U 1 1 5DA8FA83
P 5300 4400
AR Path="/5DA8F5E4/5DA8FA83" Ref="#U12"  Part="1" 
AR Path="/5DA7CA58/5DB30C0E/5DA8F5E4/5DA8FA83" Ref="#U?"  Part="1" 
AR Path="/5DA7CA58/5DA8F5E4/5DA8FA83" Ref="#U?"  Part="1" 
AR Path="/5DA7C9DA/5DAA0908/5DA8F5E4/5DA8FA83" Ref="#U?"  Part="1" 
AR Path="/5DA7C9DA/5DA8F5E4/5DA8FA83" Ref="#U?"  Part="1" 
AR Path="/5DA8B5E5/5DA8F5E4/5DA8FA83" Ref="#U?"  Part="1" 
F 0 "#U12" H 5525 4565 50  0000 C CNN
F 1 "LS" H 5525 4474 50  0000 C CNN
F 2 "" H 5350 4450 50  0001 C CNN
F 3 "" H 5350 4450 50  0001 C CNN
	1    5300 4400
	1    0    0    -1  
$EndComp
$Comp
L Sprite_ScanningMechanism:LL #U11
U 1 1 5DA8F6FC
P 5300 3200
AR Path="/5DA8F5E4/5DA8F6FC" Ref="#U11"  Part="1" 
AR Path="/5DA7CA58/5DB30C0E/5DA8F5E4/5DA8F6FC" Ref="#U?"  Part="1" 
AR Path="/5DA7CA58/5DA8F5E4/5DA8F6FC" Ref="#U?"  Part="1" 
AR Path="/5DA7C9DA/5DAA0908/5DA8F5E4/5DA8F6FC" Ref="#U?"  Part="1" 
AR Path="/5DA7C9DA/5DA8F5E4/5DA8F6FC" Ref="#U?"  Part="1" 
AR Path="/5DA8B5E5/5DA8F5E4/5DA8F6FC" Ref="#U?"  Part="1" 
F 0 "#U11" H 5575 3365 50  0000 C CNN
F 1 "LL" H 5575 3274 50  0000 C CNN
F 2 "" H 5350 3250 50  0001 C CNN
F 3 "" H 5350 3250 50  0001 C CNN
	1    5300 3200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
