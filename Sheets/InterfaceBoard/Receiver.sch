EESchema Schematic File Version 5
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Entry Wire Line
	4200 4000 4100 3900
Entry Wire Line
	5750 3550 5850 3450
Entry Wire Line
	5750 3700 5850 3600
Entry Wire Line
	5750 3850 5850 3750
Entry Wire Line
	5750 4000 5850 3900
Wire Bus Line
	4000 3900 4100 3900
Wire Wire Line
	4550 3550 4450 3550
Wire Wire Line
	4550 3700 4450 3700
Wire Bus Line
	5950 3450 5850 3450
Wire Bus Line
	5950 3600 5850 3600
Wire Bus Line
	5950 3750 5850 3750
Wire Bus Line
	5950 3900 5850 3900
Wire Wire Line
	4550 4000 4200 4000
Wire Wire Line
	5400 3550 5750 3550
Wire Wire Line
	5400 3700 5750 3700
Wire Wire Line
	5400 3850 5750 3850
Wire Wire Line
	5400 4000 5750 4000
Text Label 4200 4000 0    50   ~ 0
TR5.SIG
Text Label 5750 3550 2    50   ~ 0
TR1.SIG
Text Label 5750 3700 2    50   ~ 0
TR2.SIG
Text Label 5750 3850 2    50   ~ 0
TR3.SIG
Text Label 5750 4000 2    50   ~ 0
TR4.SIG
Text GLabel 4450 3550 0    50   Input ~ 0
5V_ESC
Text GLabel 4450 3700 0    50   Input ~ 0
GND
Text HLabel 4000 3900 0    50   Input ~ 0
TR5{SERVO}
Text HLabel 5950 3450 2    50   Input ~ 0
TR1{SERVO}
Text HLabel 5950 3600 2    50   Input ~ 0
TR2{SERVO}
Text HLabel 5950 3750 2    50   Input ~ 0
TR3{SERVO}
Text HLabel 5950 3900 2    50   Input ~ 0
TR4{SERVO}
$Comp
L Sprite_Telemetry:TR U10
U 1 1 5DA9C26F
P 4650 3450
F 0 "U10" H 4975 3615 50  0000 C CNN
F 1 "TR" H 4975 3524 50  0000 C CNN
F 2 "Sprite:TR" H 4700 3500 50  0001 C CNN
F 3 "" H 4700 3500 50  0001 C CNN
	1    4650 3450
	1    0    0    -1  
$EndComp
$EndSCHEMATC
