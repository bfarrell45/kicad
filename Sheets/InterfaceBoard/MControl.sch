EESchema Schematic File Version 5
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Connection ~ 5700 5400
NoConn ~ 3200 2000
NoConn ~ 3200 2100
NoConn ~ 3200 2300
NoConn ~ 3200 2400
NoConn ~ 3200 2500
NoConn ~ 3200 2600
NoConn ~ 3200 2700
NoConn ~ 3200 2800
NoConn ~ 3200 2900
NoConn ~ 3200 3000
NoConn ~ 3200 3100
NoConn ~ 3200 3200
NoConn ~ 3200 3300
NoConn ~ 3200 3400
NoConn ~ 3200 3500
NoConn ~ 3200 3600
NoConn ~ 3200 3700
NoConn ~ 3200 3800
NoConn ~ 3200 3900
NoConn ~ 3200 4000
NoConn ~ 3200 4100
NoConn ~ 3200 4200
NoConn ~ 3200 4300
NoConn ~ 3200 4400
NoConn ~ 3200 4500
NoConn ~ 3200 4600
NoConn ~ 3200 4700
NoConn ~ 3200 5200
NoConn ~ 3200 5300
NoConn ~ 3200 5400
NoConn ~ 3200 5500
NoConn ~ 3200 5600
NoConn ~ 3200 5700
NoConn ~ 3200 5800
NoConn ~ 3200 5900
NoConn ~ 3200 6000
NoConn ~ 5500 4300
NoConn ~ 5500 4400
NoConn ~ 5500 4500
NoConn ~ 5500 4600
NoConn ~ 5500 5000
NoConn ~ 5500 5100
NoConn ~ 5500 5200
NoConn ~ 5500 5300
NoConn ~ 5500 5500
NoConn ~ 5500 5600
NoConn ~ 5500 5700
NoConn ~ 5500 5800
NoConn ~ 5500 5900
NoConn ~ 5500 6000
NoConn ~ 5500 6100
NoConn ~ 5500 6200
Entry Wire Line
	2400 2100 2500 2200
Entry Wire Line
	2400 4700 2500 4800
Entry Wire Line
	2400 4800 2500 4900
Entry Wire Line
	2400 6000 2500 6100
Entry Wire Line
	2400 6100 2500 6200
Entry Wire Line
	6100 4800 6200 4700
Entry Wire Line
	6100 4900 6200 4800
Entry Wire Line
	6100 5300 6200 5200
Entry Wire Line
	6200 5300 6100 5400
Wire Bus Line
	2300 2100 2400 2100
Wire Bus Line
	2300 4700 2400 4700
Wire Bus Line
	2300 6000 2400 6000
Wire Bus Line
	2400 4700 2400 4800
Wire Bus Line
	2400 6100 2300 6100
Wire Wire Line
	3200 1900 3100 1900
Wire Wire Line
	5700 5300 5700 5400
Wire Bus Line
	6200 4700 6300 4700
Wire Bus Line
	6200 4700 6200 4800
Wire Bus Line
	6300 5200 6200 5200
Wire Bus Line
	6300 5300 6200 5300
Wire Wire Line
	5500 5400 5700 5400
Wire Wire Line
	5700 5300 6100 5300
Wire Wire Line
	5700 5400 6100 5400
Wire Wire Line
	5500 4800 6100 4800
Wire Wire Line
	5500 4900 6100 4900
Wire Wire Line
	2500 2200 3200 2200
Wire Wire Line
	2500 4800 3200 4800
Wire Wire Line
	2500 4900 3200 4900
Wire Wire Line
	2500 6100 3200 6100
Wire Wire Line
	2500 6200 3200 6200
Wire Wire Line
	2300 5000 3200 5000
Wire Wire Line
	2300 5100 3200 5100
Text Label 2500 2200 0    50   ~ 0
RPM.SIG
Text Label 2500 4800 0    50   ~ 0
SCL
Text Label 2500 4900 0    50   ~ 0
SDA
Text Label 2500 6100 0    50   ~ 0
STEERING.SIG
Text Label 2500 6200 0    50   ~ 0
THROTTLE.SIG
Text Label 6100 4800 2    50   ~ 0
D+
Text Label 6100 4900 2    50   ~ 0
D-
Text Label 6100 5300 2    50   ~ 0
VBUS
Text Label 6100 5400 2    50   ~ 0
RPM.VCC
Text GLabel 3100 1900 0    50   Input ~ 0
GND
Text HLabel 2300 2100 0    50   Input ~ 0
RPM{SERVO}
Text HLabel 2300 4700 0    50   Input ~ 0
{I2C}
Text HLabel 2300 5000 0    50   Input ~ 0
INT
Text HLabel 2300 5100 0    50   Input ~ 0
RST
Text HLabel 2300 6000 0    50   Input ~ 0
STEERING{SERVO}
Text HLabel 2300 6100 0    50   Input ~ 0
THROTTLE{SERVO}
Text HLabel 6300 4700 2    50   Input ~ 0
{USB}
Text HLabel 6300 5200 2    50   Input ~ 0
{USB}
Text HLabel 6300 5300 2    50   Input ~ 0
RPM{SERVO}
$Comp
L teensy:Teensy3.5 U7
U 1 1 5DC07818
P 4350 4050
F 0 "U7" H 4350 6793 60  0000 C CNN
F 1 "Teensy3.5" H 4350 6686 60  0000 C CNN
F 2 "teensy:Teensy35_36" H 4350 6579 60  0000 C CNN
F 3 "https://www.pjrc.com/teensy/card8a_rev2.pdf" H 4350 6472 60  0000 C CNN
F 4 "https://www.pjrc.com/teensy/pinout.html" H 4350 6373 50  0000 C CNN "Pinouts"
	1    4350 4050
	1    0    0    -1  
$EndComp
$EndSCHEMATC
