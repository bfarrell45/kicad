EESchema Schematic File Version 5
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Entry Wire Line
	4650 3750 4750 3850
Entry Wire Line
	4650 3850 4750 3950
Wire Wire Line
	3650 3600 3650 3500
Wire Bus Line
	4550 3750 4650 3750
Wire Bus Line
	4650 3750 4650 3850
Wire Wire Line
	7450 3950 7550 3950
Wire Wire Line
	7550 3950 7550 3850
Wire Wire Line
	7650 3850 7650 3950
Wire Wire Line
	7650 3950 7750 3950
Wire Wire Line
	4900 3550 5050 3550
Wire Wire Line
	4900 3750 5050 3750
Wire Wire Line
	5050 3650 4900 3650
Wire Wire Line
	3650 3500 3450 3500
Wire Wire Line
	3650 4200 3650 4400
Wire Wire Line
	3650 4400 3850 4400
Wire Wire Line
	5850 3850 6050 3850
Wire Wire Line
	4750 3850 5050 3850
Wire Wire Line
	4750 3950 5050 3950
Wire Wire Line
	5850 3750 6200 3750
Wire Wire Line
	6200 3650 5850 3650
Wire Wire Line
	6250 3950 5850 3950
Wire Wire Line
	4550 4050 5050 4050
Wire Notes Line
	7200 3500 7200 4050
Wire Notes Line
	8300 4050 8300 3500
Wire Notes Line
	3350 4500 4000 4500
Wire Notes Line
	4000 3300 3350 3300
Wire Notes Line
	7200 4050 8300 4050
Wire Notes Line
	8300 3500 7200 3500
Wire Notes Line
	3350 3300 3350 4500
Wire Notes Line
	4000 4500 4000 3300
Text Notes 3350 3250 0    50   ~ 0
Power LED
Text Notes 4650 4500 0    50   ~ 0
No pullups are necessary on SDA/SCL,\nas 10k pullups are built into the breakout.
Text Notes 7200 3450 0    50   ~ 0
Communication Mode jumper\nOpen: I2C (default)\nClosed: UART (SCL->RX, SDA->TX)
Text Label 3450 3500 0    50   ~ 0
3V3
Text Label 3850 4400 2    50   ~ 0
GND
Text Label 4900 3650 0    50   ~ 0
3V3
Text Label 5050 3550 2    50   ~ 0
VCC
Text Label 5050 3750 2    50   ~ 0
GND
Text Label 5050 3850 2    50   ~ 0
SDA
Text Label 5050 3950 2    50   ~ 0
SCL
Text Label 5900 3650 0    50   ~ 0
PS0
Text Label 5900 3750 0    50   ~ 0
PS1
Text Label 7450 3950 2    50   ~ 0
PS1
Text Label 7750 3950 0    50   ~ 0
3V3
Text GLabel 4900 3550 0    50   Input ~ 0
5V
Text GLabel 4900 3750 0    50   Input ~ 0
GND
Text HLabel 4550 3750 0    50   Input ~ 0
{I2C}
Text HLabel 4550 4050 0    50   Input ~ 0
RST
Text HLabel 6050 3850 2    50   Input ~ 0
INT
$Comp
L Connector:TestPoint TP1
U 1 1 5DB223B6
P 6250 3950
F 0 "TP1" V 6204 4138 50  0000 L CNN
F 1 "TestPoint" V 6295 4138 50  0000 L CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D2.0mm" H 6450 3950 50  0001 C CNN
F 3 "~" H 6450 3950 50  0001 C CNN
	1    6250 3950
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R30
U 1 1 5DD1EA34
P 3650 3750
F 0 "R30" H 3718 3796 50  0000 L CNN
F 1 "R_US" H 3718 3705 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3690 3740 50  0001 C CNN
F 3 "~" H 3650 3750 50  0001 C CNN
	1    3650 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D17
U 1 1 5DD112DC
P 3650 4050
F 0 "D17" V 3689 3933 50  0000 R CNN
F 1 "RED" V 3598 3933 50  0000 R CNN
F 2 "LED_SMD:LED_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3650 4050 50  0001 C CNN
F 3 "~" H 3650 4050 50  0001 C CNN
	1    3650 4050
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J8
U 1 1 5DB1F127
P 6400 3650
F 0 "J8" H 6480 3642 50  0000 L CNN
F 1 "Conn_01x02" H 6480 3551 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 6400 3650 50  0001 C CNN
F 3 "~" H 6400 3650 50  0001 C CNN
	1    6400 3650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J9
U 1 1 5DD13CD7
P 7550 3650
F 0 "J9" V 7514 3462 50  0000 R CNN
F 1 "Conn_01x02" V 7423 3462 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7550 3650 50  0001 C CNN
F 3 "~" H 7550 3650 50  0001 C CNN
	1    7550 3650
	0    -1   -1   0   
$EndComp
$Comp
L Sprite_Sensors:SI U8
U 1 1 5DB2DD32
P 5150 3450
F 0 "U8" H 5450 3615 50  0000 C CNN
F 1 "SI" H 5450 3524 50  0000 C CNN
F 2 "Sprite:SI" H 5150 3450 50  0001 C CNN
F 3 "" H 5150 3450 50  0001 C CNN
	1    5150 3450
	1    0    0    -1  
$EndComp
$EndSCHEMATC
