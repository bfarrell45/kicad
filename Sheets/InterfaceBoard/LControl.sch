EESchema Schematic File Version 5
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Connection ~ 5650 5400
NoConn ~ 3150 2300
NoConn ~ 3150 2400
NoConn ~ 3150 2900
NoConn ~ 3150 3000
NoConn ~ 3150 3100
NoConn ~ 3150 3200
NoConn ~ 3150 3300
NoConn ~ 3150 3400
NoConn ~ 3150 3500
NoConn ~ 3150 3600
NoConn ~ 3150 3700
NoConn ~ 3150 3800
NoConn ~ 3150 3900
NoConn ~ 3150 4000
NoConn ~ 3150 4100
NoConn ~ 3150 4200
NoConn ~ 3150 4300
NoConn ~ 3150 4400
NoConn ~ 3150 4500
NoConn ~ 3150 4600
NoConn ~ 3150 4700
NoConn ~ 3150 4800
NoConn ~ 3150 4900
NoConn ~ 3150 5000
NoConn ~ 3150 5100
NoConn ~ 3150 5200
NoConn ~ 3150 5300
NoConn ~ 3150 5400
NoConn ~ 3150 5500
NoConn ~ 3150 5600
NoConn ~ 3150 5700
NoConn ~ 3150 5800
NoConn ~ 3150 5900
NoConn ~ 3150 6000
NoConn ~ 3150 6100
NoConn ~ 3150 6200
NoConn ~ 5450 4300
NoConn ~ 5450 4400
NoConn ~ 5450 4500
NoConn ~ 5450 4600
NoConn ~ 5450 4700
NoConn ~ 5450 5000
NoConn ~ 5450 5100
NoConn ~ 5450 5200
NoConn ~ 5450 5300
NoConn ~ 5450 5500
NoConn ~ 5450 5600
NoConn ~ 5450 5700
NoConn ~ 5450 5800
NoConn ~ 5450 5900
NoConn ~ 5450 6000
NoConn ~ 5450 6100
NoConn ~ 5450 6200
Entry Wire Line
	2700 1900 2800 2000
Entry Wire Line
	2700 2000 2800 2100
Entry Wire Line
	2700 2400 2800 2500
Entry Wire Line
	2700 2700 2800 2600
Entry Wire Line
	2700 2800 2800 2700
Entry Wire Line
	2700 2900 2800 2800
Entry Wire Line
	5950 4800 6050 4700
Entry Wire Line
	5950 4900 6050 4800
Entry Wire Line
	5950 5300 6050 5200
Entry Wire Line
	6050 5300 5950 5400
Wire Bus Line
	2600 1900 2700 1900
Wire Bus Line
	2600 2400 2700 2400
Wire Bus Line
	2700 2000 2700 1900
Wire Bus Line
	2700 2900 2600 2900
Wire Wire Line
	5650 5300 5650 5400
Wire Bus Line
	6050 4700 6150 4700
Wire Bus Line
	6050 4700 6050 4800
Wire Bus Line
	6150 5200 6050 5200
Wire Bus Line
	6150 5300 6050 5300
Wire Bus Line
	2700 2700 2700 2900
Wire Wire Line
	5450 5400 5650 5400
Wire Wire Line
	5650 5300 5950 5300
Wire Wire Line
	5650 5400 5950 5400
Wire Wire Line
	2800 2000 3150 2000
Wire Wire Line
	2800 2100 3150 2100
Wire Wire Line
	2800 2500 3150 2500
Wire Wire Line
	2800 2600 3150 2600
Wire Wire Line
	2800 2700 3150 2700
Wire Wire Line
	2800 2800 3150 2800
Wire Wire Line
	5450 4800 5950 4800
Wire Wire Line
	5450 4900 5950 4900
Wire Wire Line
	2600 2200 3150 2200
Text Label 2800 2000 0    50   ~ 0
RX
Text Label 2800 2100 0    50   ~ 0
TX
Text Label 2800 2500 0    50   ~ 0
SIG
Text Label 2800 2600 0    50   ~ 0
IDX
Text Label 2800 2700 0    50   ~ 0
CHA
Text Label 2800 2800 0    50   ~ 0
CHB
Text Label 5950 4800 2    50   ~ 0
D+
Text Label 5950 4900 2    50   ~ 0
D-
Text Label 5950 5300 2    50   ~ 0
VBUS
Text Label 5950 5400 2    50   ~ 0
VCC
Text GLabel 3150 1900 0    50   Input ~ 0
GND
Text HLabel 2600 1900 0    50   Input ~ 0
{UART}
Text HLabel 2600 2200 0    50   Input ~ 0
SYN
Text HLabel 2600 2400 0    50   Input ~ 0
{SERVO}
Text HLabel 2600 2900 0    50   Input ~ 0
{QE}
Text HLabel 6150 4700 2    50   Input ~ 0
{USB}
Text HLabel 6150 5200 2    50   Input ~ 0
{USB}
Text HLabel 6150 5300 2    50   Input ~ 0
{QE}
$Comp
L teensy:Teensy3.6 U6
U 1 1 5DB2431E
P 4300 4050
F 0 "U6" H 4300 6489 60  0000 C CNN
F 1 "Teensy3.6" H 4300 6382 60  0000 C CNN
F 2 "teensy:Teensy35_36" H 5150 6400 60  0000 C CNN
F 3 "" H 4300 4100 60  0000 C CNN
	1    4300 4050
	1    0    0    -1  
$EndComp
$EndSCHEMATC
