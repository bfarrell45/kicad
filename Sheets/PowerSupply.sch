EESchema Schematic File Version 5
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 11 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Connection ~ 5000 3650
Wire Wire Line
	5100 3850 5200 3850
Wire Bus Line
	5100 4050 5200 4050
Wire Wire Line
	6500 3650 6600 3650
Wire Bus Line
	6500 3850 6600 3850
Wire Bus Line
	6500 4050 6600 4050
Wire Wire Line
	6600 3750 6500 3750
Wire Wire Line
	5000 3650 5200 3650
Wire Wire Line
	4750 3650 5000 3650
Wire Wire Line
	5000 3300 5000 3650
Wire Wire Line
	5000 3300 6600 3300
Text HLabel 5100 3850 0    50   Input ~ 0
P1
Text HLabel 5100 4050 0    50   Input ~ 0
P5{USB}
Text HLabel 6600 3300 2    50   Input ~ 0
P0
Text HLabel 6600 3650 2    50   Input ~ 0
P2
Text HLabel 6600 3750 2    50   Input ~ 0
P3
Text HLabel 6600 3850 2    50   Input ~ 0
P4{PWR}
Text HLabel 6600 4050 2    50   Input ~ 0
P6{MOLEX}
$Comp
L Sprite_PowerSupply:PB #U4
U 1 1 5DA8B764
P 4150 3550
F 0 "#U4" H 4400 3715 50  0000 C CNN
F 1 "PB" H 4400 3624 50  0000 C CNN
F 2 "" H 4150 3550 50  0001 C CNN
F 3 "" H 4150 3550 50  0001 C CNN
	1    4150 3550
	1    0    0    -1  
$EndComp
$Comp
L Sprite_PowerSupply:PS #U5
U 1 1 5DA8B939
P 5300 3550
F 0 "#U5" H 5800 3715 50  0000 C CNN
F 1 "PS" H 5800 3624 50  0000 C CNN
F 2 "" H 5350 3600 50  0001 C CNN
F 3 "" H 5350 3600 50  0001 C CNN
	1    5300 3550
	1    0    0    -1  
$EndComp
$EndSCHEMATC
